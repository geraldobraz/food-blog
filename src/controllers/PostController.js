const Post = require('../models/Post');

const DATE_FORMAT_OPTIONS = { year: 'numeric', month: 'long', day: 'numeric' };

const getFormattedPost = (post) => {
  post['date'] = post.createdAt.toLocaleDateString('en-US', DATE_FORMAT_OPTIONS);

  post['imgSrc'] = post.image && post.image.data 
      ? `data:image/${post.image.contentType};base64,${post.image.data.toString('base64')}`
      : '/images/default-image.jpg'; 
  return post;
}

module.exports = {
  async listAll() {
    const posts = await Post.find({});
    return posts.map(post => getFormattedPost(post));
  },

  async findById(id) {
    const post = await Post.findById(id);
    return getFormattedPost(post);
  },

  async findBySlug(slug) {
    const post = await Post.findOne({ slug });
    return getFormattedPost(post);
  },

  create(newPost) {
    return Post.create(newPost)
  },

  update(updatePost) {
    return Post.findByIdAndUpdate(updatePost.id, updatePost, { useFindAndModify: false })
  },

  delete(id) {
    return Post.findByIdAndDelete(id)
  },
};