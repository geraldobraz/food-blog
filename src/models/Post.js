const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    title: String,
    slug: String,
    content: String,
    image: { data: Buffer, contentType: String },
}, { timestamps: true });

module.exports = mongoose.model('Post', PostSchema);