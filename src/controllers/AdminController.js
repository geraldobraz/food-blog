const Admin = require('../models/Admin');

module.exports = {
  async get(email, password) {
    return Admin.findOne({ email, password });
  },

  async create(email, password) {
    let admin = await Admin.findOne({ email });
      if (!admin) {  
          admin = await Admin.create({
              email,
              password,
          });
      }

      return admin;
  },
};