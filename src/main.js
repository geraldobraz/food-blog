const express = require('express');
const { check, validationResult } = require('express-validator');
const session = require('express-session');
const fileupload = require('express-fileupload');
const path = require ('path');
const mongoose = require('mongoose');

const AdminController = require('./controllers/AdminController');
const PostController = require('./controllers/PostController');

mongoose.connect('mongodb://localhost:27017/food-blog',
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log('Connected to database');
    }
  },
);

const app = express();

app.set('views', path.join (__dirname, 'views'));
app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: false }));

app.use(session({
  secret : "d340ac8aae2e7f98531a7fe069b2052a",
  resave : false,
  saveUninitialized : true
}));

app.use(fileupload());

/************************* Admin Dashboard *************************/

const authMiddleware = (req, res, next) => {
  if (req.session.userLoggedIn) {
    next();
  } else {
    res.redirect('/admin/login');
  }
};

const loginMiddleware = (req, res, next) => {
  if (req.session.userLoggedIn) {
    res.redirect('/admin');
  } else {
    next();
  }
};

// Login
app.get('/admin/login', loginMiddleware, (req, res) => {
  res.render('admin/login');
});

app.get('/admin/signin', (req, res) => {
  res.render('admin/signin');
});

app.post('/admin/signin', [
    check('email', 'Email is not valid')
        .isEmail()
        .normalizeEmail(),
    check('password', 'This password must be 6+ characters long')
      .exists()
      .isLength({ min: 6 }),
  ], async (req, res)=> {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.render('admin/signin', { fieldErrors: errors.array() });
    } else {
      const { email, password } = req.body;
      const admin = await AdminController.create(email, password);

      if (admin) {
        req.session.userLoggedIn = true;
        req.session.admin = admin;
        res.redirect('/admin');
      } else {
        res.render('admin/signin', { loginError: true });
      }
    }
})

app.post('/admin/login', [
    check('email', 'Email is not valid')
        .isEmail()
        .normalizeEmail(),
    check('password', 'This password must be 6+ characters long')
      .exists()
      .isLength({ min: 6 }),
  ], async (req, res)=> {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.render('admin/login', { fieldErrors: errors.array() });
    } else {
      const { email, password } = req.body;
      const admin = await AdminController.get(email, password);
      if (admin) {
        req.session.userLoggedIn = true;
        req.session.admin = admin;
        res.redirect('/admin');
      } else {
        res.render('admin/login', { loginError: true });
      }
    }
})

// Logout

app.get('/admin/logout', (req, res) => {
  req.session.userLoggedIn = false;
  req.session.admin = null;

  res.redirect('/home');
});

// Dashboard

app.get('/admin', (req, res) => {
  res.redirect('/admin/dashboard');
});

app.get('/admin/dashboard', authMiddleware, async (req, res) => {
  const posts = await PostController.listAll();

  res.render('admin/dashboard', { posts });
});

// Edit/Create/Delete Post

app.get('/admin/post', authMiddleware, async (req, res) => {
  const { id } = req.query;

  let post = {};
  if (id) {
    post = await PostController.findById(id);
  }

  res.render('admin/post', { post });
});

app.post('/admin/post', [
    check('title', 'Title cannot be empty')
      .notEmpty(),
    check('slug', 'Slug cannot be empty')
      .notEmpty()
  ], async (req, res)=> {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.render('admin/post', { fieldErrors: errors.array() });
    } else {
      const { id, title, slug, content } = req.body;
      
      const post = {
        title,
        slug,
        content
      }
      if (id) {
        post.id = id;
      }
      if (req.files) {
        const image = req.files.image;
        post.image = {
          data: image.data,
          contentType: image.mimetype,
        };
      }

      if (id) {
        await PostController.update(post)
      } else {
        await PostController.create(post)
      }

      res.redirect('/admin');
    }
});

app.post('/admin/post/delete/:id', authMiddleware, async (req, res) => {
  const id = req.params.id;
  await PostController.delete(id);

  res.redirect('/admin');
});

/****************************** Blog ******************************/

app.get('/', (req, res) => {
  res.redirect('/home');
});

app.get('/home', async (req, res) => {
  const posts = await PostController.listAll();
  
  res.render('blog/home', { posts, admin: req.session.admin });
});

app.get('/post/:slug', async (req, res) => {
  const slug = req.params.slug;

  const post = await PostController.findBySlug(slug);

  res.render('blog/post', { post, admin: req.session.admin });
});


app.listen(8080, () => {
  console.log('Food Blog');
  console.log('🚀 Server started on port 8080!');
});