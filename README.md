# 🍽 Food Blog #

Application for a food blog. The user can create/edit/delete posts from the admin dashboard and visualize all posts on the home page.

## 🚨 Prerequisites ##

* Node
* Npm
* MongoDB

## 🚀 Getting Started

1. Install dependencies

```
npm install
```

2. Run application

```
npm start
```

3. Go to [http://localhost:8080/](http://localhost:8080/)

## 🛠 Built With ##

* [Node](https://nodejs.org/) - JavaScript runtime environment
* [Npm](https://www.npmjs.com/) - Package Manager
* [Express](https://expressjs.com/) - Web framework used NodeJS
* [EJS](https://ejs.co/) - Library for embedded JavaScript templating
* [MongoDB](https://www.mongodb.com/) - NoSQL database

## 🎖 Author ##
**Geraldo Braz** - [@geraldobraz](https://bitbucket.org/geraldobraz/)

## 📝 License ##

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
The MIT licensed was chosen because it a simple permissive license that allows this code to be used by others as long as they preserve the copyright and license notices.
